Minimal demo of a Qt issue
==========================

For investigation of an issue with Flickable on old iPad/iOS HW: it certainly used to work, but now Flickable no longer flicks on old HW/OS (not sure which is the more sifgnificant factor).  Issue seen with Qt5.7 and Qt5.9.4.

Submitted as https://bugreports.qt.io/browse/QTBUG-66417

Status
------

* Linux and mac (OSX) builds work as expected: shows a patchwork quilt of randomly coloured squares which can be flicked.
* "Newer" iPad ("MD785B/A") running iOS 11.2.5 : works as expected (all of Qt5.7, 5.9.4 and 5.10.0 tried and good).
* "Old" iPad ("MD510B/A") running iOS 10.3.3 (and not upgradeable to 11): shows the "quilt"... but it just cannot be flicked!  Tried with Qt 5.7 and 5.9.4 (tried 5.10.0 too but xcode said something about the old iPad's architecture not being supported and needing to change build settings... not investigated that further yet).

All OSX/iOS builds done on an early 2016 Macbook running up-to-date HighSierra and xcode 9.2.  This issue was first noticed in an xcode8 build but I've since updated the Macs I have to xcode9 (where the issue persists).

The crazy thing is: in the past Flickable has worked absolutely fine on this iPad and this (and older) iOS versions... there's a working app in the appstore built with Qt5.7 and xcode8 where it works fine.  The only thing I'm aware of changing is that before the first build where the issue was noticed xcode briefly flashed up an "xcode is updating" message... my guess would be Apple have changed something in some iOS SDK or something like that; not sure how to investigate further though.

Build
-----

NB First you will need to change, in whichever build script you use:

* The path to your Qt dir (and possibly version) in those scripts.  
* For `./MAKE-ios`, the `teamsetting.value` to your Apple Developer ID (or do I mean team ID?)
* For `./MAKE-android`, the paths set for `ANDROID_NDK_ROOT` and `ANDROID_SDK_ROOT` will probably need changing.

Build with:

* **OSX**: `./MAKE-mac` (builds `qtissue.app`)
* **iOS**: `./MAKE-ios` (builds `qtissue.xcodeproj` which should then be built with and deployed from xcode; xcode8 assumed).
* **Linux**: `./MAKE-linux` builds `./qtissue` (reference Debian 8.9 (Jessie); NB *not* expected to work with Debian's Qt4!)

TODO
----

Not yet tried Android.  Be surprised if it didn't work.

* **Android**: `./MAKE-android` builds `deploy/bin/QtApp-debug.apk` which can be deployed to a device using adb install.
    * Note that `main.cpp` contains some android-specific code to redirect Qt logging (whether from use of things like `qInfo() << ` in C++ or `console.log` in QML) to android's logging where it can be observed with `adb logcat` (probably also with `| grep qtissue` to limit the spew).  However currently the app code doesn't actually include anything which would produce any such messages.

Use `./MAKE-clean` to cleanup when switching between target architectures to nuke any build-product detritus.

Notes
-----

* Besides the build scripts and `.pro` file and docs, there's not really anything more to this app than what's in `main.cpp` and `main.qml`.  The `qtissue.qrc` just ensures the qml is baked into the app.
* At time of writing, build scripts set to use Qt 5.7.
