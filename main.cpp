#include <QGuiApplication>
#include <QtGlobal>
#include <QQuickView>

const char*const applicationName="qtissue";

#ifdef ANDROIDQUIRKS
// Redirects Qt logging to android logging
// see https://stackoverflow.com/q/45458064/24283

#include <android/log.h>

void myMessageHandler(
  QtMsgType type,
  const QMessageLogContext& context,
  const QString& msg
) {
  QString report=msg;
  if (context.file && !QString(context.file).isEmpty()) {
    report+=" in file ";
    report+=QString(context.file);
    report+=" line ";
    report+=QString::number(context.line);
  }
  if (context.function && !QString(context.function).isEmpty()) {
    report+=+" function ";
    report+=QString(context.function);
  }
  const char*const local=report.toLocal8Bit().constData();
  switch (type) {
  case QtDebugMsg:
    __android_log_write(ANDROID_LOG_DEBUG,applicationName,local);
    break;
  case QtInfoMsg:
    __android_log_write(ANDROID_LOG_INFO,applicationName,local);
    break;
  case QtWarningMsg:
    __android_log_write(ANDROID_LOG_WARN,applicationName,local);
    break;
  case QtCriticalMsg:
    __android_log_write(ANDROID_LOG_ERROR,applicationName,local);
    break;
  case QtFatalMsg:
  default:
    __android_log_write(ANDROID_LOG_FATAL,applicationName,local);
    abort();    
  }
}
#endif

int main(int argc,char* argv[]) {

  QGuiApplication app(argc,argv);  
#ifdef ANDROIDQUIRKS
  qInstallMessageHandler(myMessageHandler);
#endif
  app.setApplicationName(applicationName);
  app.setOrganizationName("Qt fanclub");
  app.setOrganizationDomain("com.example");

  QQuickView viewer;
  viewer.setResizeMode(QQuickView::SizeRootObjectToView);
  viewer.resize(QSize(640,480));
  viewer.setSource(QUrl("qrc:///main.qml"));
  viewer.show();

  return app.exec();
}
